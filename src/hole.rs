use crate::core::child_cores;
use crate::{Core, GameTimer, GetTypedNode, OrphanInstanceWatcher, PauseMenu, Player, Settings};
use gdnative::api::{AnimationPlayer, Area2D, CollisionObject2D, Label, Node};
use gdnative::nativescript::{Map, MapMut};
use gdnative::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Default, NativeClass)]
#[inherit(Node)]
#[register_with(Self::register)]
pub struct Hole {
    cores: Vec<Instance<Core, Shared>>,
    timer: GameTimer,
    player: Option<Instance<Player, Shared>>,
    start_transform: Transform2D,
    start_gravpack_fuel: f32,
    animation_players: Vec<Ref<AnimationPlayer>>,
    settings: Rc<RefCell<Settings>>,

    hole_time: Option<Ref<Label>>,
    fuel_left: Option<Ref<Label>>,
    victory: Option<Ref<Area2D>>,
    pause_menu: Option<OrphanInstanceWatcher<PauseMenu>>,
}

#[methods]
impl Hole {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "victory",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "exit",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "pause",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "unpause",
            args: &[],
        });
    }

    fn new(_owner: &Node) -> Self {
        Default::default()
    }

    pub fn set_settings(&mut self, settings: Rc<RefCell<Settings>>) {
        self.settings = settings.clone();
        let player = self.player.as_ref().unwrap();
        let player = unsafe { player.assume_safe() };
        player
            .map_mut(|player, _| player.set_settings(settings.clone()))
            .unwrap();
        if let Some(pause_menu) = self.pause_menu.as_ref() {
            let pause_menu = unsafe { pause_menu.assume_safe() };
            pause_menu
                .map_mut(move |script, _| script.set_settings(settings))
                .unwrap();
        }
    }

    /// Find the Area2D named Victory in the tree, if there is one.
    fn find_victory(node: &Node) -> Option<Ref<Area2D>> {
        if let Some(victory) = node.get_node_or_null("Victory") {
            let victory = unsafe { victory.assume_safe() };
            if let Some(victory) = victory.cast::<Area2D>() {
                return Some(victory.claim());
            }
        }
        for child in node.get_children().iter() {
            if let Some(child) = child.try_to_object::<Node>() {
                let child = unsafe { child.assume_safe() };
                if let Some(victory) = Hole::find_victory(&child) {
                    return Some(victory);
                }
            }
        }
        None
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Node>) {
        let node: TRef<Node> = owner.upcast();
        self.cores = child_cores(&owner.upcast());
        let player: RefInstance<Player, Shared> = unsafe { node.get_typed_instance("Player") };
        self.start_transform = player.base().get_transform();
        self.start_gravpack_fuel = player.script().map(Player::get_gravpack_fuel).unwrap();
        player
            .base()
            .connect(
                "gravpack_fuel_change",
                owner,
                "update_fuel",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
        player
            .map_mut(|player, _| player.set_air_parent(owner.claim()))
            .unwrap();
        self.player = Some(player.claim());
        if let Some(animations) = node.get_node_or_null("Animations") {
            let animations = unsafe { animations.assume_safe() };
            for child in animations.get_children().iter() {
                if let Some(child) = child.try_to_object::<AnimationPlayer>() {
                    self.animation_players.push(child);
                }
            }
        }
        self.hole_time = Some(unsafe { node.get_claimed_node("Hud/HoleTime") });
        self.fuel_left = Some(unsafe { node.get_claimed_node("Hud/FuelLeft") });

        let pause_menu: RefInstance<PauseMenu, _> = unsafe { node.get_typed_instance("PauseMenu") };
        pause_menu
            .map_mut(|script, base| {
                owner.remove_child(base);
                script.set_settings(self.settings.clone());
            })
            .unwrap();
        self.pause_menu = Some(pause_menu.claim().into());

        self.victory = Hole::find_victory(&node);

        self.restart(owner);
    }

    #[export]
    fn _process(&mut self, owner: TRef<Node>, _delta: f32) {
        if Input::godot_singleton().is_action_just_pressed("pause") {
            owner.emit_signal("pause", &[]);
            let pause_menu = unsafe { self.pause_menu.as_ref().unwrap().assume_safe() };
            pause_menu
                .map(|script, base| {
                    owner.add_child(base, false);
                    script.focus(base);
                })
                .unwrap();

            let tree = owner.get_tree().unwrap();
            let tree = unsafe { tree.assume_safe() };
            tree.set_pause(true);
            self.timer.pause().unwrap();
        }

        let player = unsafe { self.player.as_ref().unwrap().assume_safe() };
        let victory = unsafe { self.victory.unwrap().assume_safe() };
        let vector_to_victory = victory.global_position() - player.base().global_position();
        let angle_to_victory = vector_to_victory.angle_from_x_axis();
        player
            .map(|script, _| script.set_victory_angle(angle_to_victory))
            .unwrap();
    }

    #[export]
    fn _physics_process(&self, _owner: TRef<Node>, _delta: f32) {
        let player = self.player.as_ref().unwrap();
        let player_position = {
            let player = unsafe { player.assume_safe() };
            player.base().global_position()
        };
        let net_acceleration = self.cores.iter().fold(Vector2::zero(), |acc, core| {
            let core = unsafe { core.assume_safe() };
            acc + core
                .map(|core, owner| core.pull_on_point(owner.global_position(), player_position))
                .unwrap()
        });
        let player = unsafe { player.assume_safe() };
        player
            .script()
            .map_mut(|player| player.set_net_core_acceleration(net_acceleration))
            .unwrap();
    }

    #[export]
    fn unpause(&mut self, owner: &Node) {
        if self.timer.paused() {
            let tree = owner.get_tree().unwrap();
            let tree = unsafe { tree.assume_safe() };
            tree.set_pause(false);
            let pause_menu = unsafe { self.pause_menu.as_ref().unwrap().assume_safe() };
            owner.remove_child(pause_menu.base());
            self.timer.unpause().unwrap();
            owner.emit_signal("unpause", &[]);
        }
    }

    #[export]
    fn exit(&mut self, owner: &Node) {
        self.unpause(owner);
        owner.emit_signal("exit", &[]);
    }

    /// Update the run time for the HUD.
    #[export]
    fn run_time(&self, _owner: &Node) {
        if !self.timer.paused() {
            // Subtract the duration, capping it at zero
            let runtime = self.timer.runtime();
            let hole_time = unsafe { self.hole_time.unwrap().assume_safe() };
            hole_time.set_text(runtime.to_string());
        }
    }

    #[export]
    fn victory(&self, owner: TRef<Node>, _body: Ref<Node>) {
        // We have to defer this call to make sure it doesn't happen during a physics process.
        unsafe {
            owner.call_deferred("emit_signal", &[Variant::from_str("victory")]);
        }
    }

    /// Death method.
    /// Currently just delegates to [restart], but that might change in the future to keep track of
    /// deaths.
    #[export]
    fn death(&mut self, owner: TRef<Node>, _body: Ref<CollisionObject2D>) {
        // We need to call deferred, because the player might still be borrowed in _physics_process
        unsafe {
            owner.call_deferred("restart", &[]);
        }
    }

    #[export]
    fn restart(&mut self, owner: TRef<Node>) {
        // Make sure hole is not paused.
        self.unpause(&owner);
        let player = self.player.as_ref().unwrap();
        let player = unsafe { player.assume_safe() };
        player
            .map_mut(|script, base| {
                script.set_velocity(Vector2::zero());
                script.set_gravpack_fuel(self.start_gravpack_fuel);
                base.set_transform(self.start_transform);
            })
            .unwrap();

        for animation_player in &self.animation_players {
            let animation_player = unsafe { animation_player.assume_safe() };
            animation_player.stop(true);
            animation_player.play("animation", -1.0, 1.0, false);
        }

        self.timer = GameTimer::default();
        self.update_fuel(owner, self.start_gravpack_fuel);
    }

    #[export]
    fn update_fuel(&self, _owner: TRef<Node>, gravpack_fuel_left: f32) {
        let fuel_left = self.fuel_left.unwrap();
        let fuel_left = unsafe { fuel_left.assume_safe() };
        fuel_left.set_text(format!("{:.0}", gravpack_fuel_left));
    }
}
