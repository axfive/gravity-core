use gdnative::prelude::*;

pub trait GetTypedNode {
    unsafe fn get_typed_node<T, P>(&self, path: P) -> TRef<'_, T, Shared>
    where
        T: GodotObject + SubClass<Node>,
        P: Into<NodePath>;
    unsafe fn get_claimed_node<T, P>(&self, path: P) -> Ref<T, Shared>
    where
        T: GodotObject + SubClass<Node>,
        P: Into<NodePath>;
    unsafe fn get_typed_instance<C, P>(&self, path: P) -> RefInstance<'_, C, Shared>
    where
        C: NativeClass,
        C::Base: GodotObject + SubClass<Node>,
        P: Into<NodePath>;
    unsafe fn get_claimed_instance<C, P>(&self, path: P) -> Instance<C, Shared>
    where
        C: NativeClass,
        C::Base: GodotObject + SubClass<Node>,
        P: Into<NodePath>;
}

impl GetTypedNode for Node {
    unsafe fn get_typed_node<T, P>(&self, path: P) -> TRef<'_, T, Shared>
    where
        T: GodotObject + SubClass<Node>,
        P: Into<NodePath>,
    {
        self.get_node(path.into())
            .expect("node should exist")
            .assume_safe()
            .cast()
            .expect("node needs to be the correct type")
    }

    unsafe fn get_claimed_node<T, P>(&self, path: P) -> Ref<T, Shared>
    where
        T: GodotObject + SubClass<Node>,
        P: Into<NodePath>,
    {
        let node = self.get_typed_node::<T, _>(path);
        node.claim()
    }

    unsafe fn get_typed_instance<C, P>(&self, path: P) -> RefInstance<'_, C, Shared>
    where
        C: NativeClass,
        C::Base: GodotObject + SubClass<Node>,
        P: Into<NodePath>,
    {
        let node: TRef<C::Base> = self.get_typed_node(path);
        node.cast_instance()
            .expect("Must be able to cast node as the instance")
    }

    unsafe fn get_claimed_instance<C, P>(&self, path: P) -> Instance<C, Shared>
    where
        C: NativeClass,
        C::Base: GodotObject + SubClass<Node>,
        P: Into<NodePath>,
    {
        let instance: RefInstance<C, Shared> = self.get_typed_instance(path);
        instance.claim()
    }
}
